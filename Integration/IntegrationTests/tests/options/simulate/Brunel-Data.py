
from Gaudi.Configuration import *
from Configurables import Brunel, OutputStream

EventSelector().Input = ["DATAFILE='PFN:BooleTest.digi' TYP='POOL_ROOTTREE' OPT='READ'"#,
#                         "DATAFILE='PFN:BooleTest.mdf' SVC='LHCb::RawDataCnvSvc' OPT='READ'"
                         ]

Brunel().InputType = "DIGI" # implies also Brunel().Simulation = True
Brunel().WithMC    = True   # implies also Brunel().Simulation = True
#Brunel().OutputType="XDST"

#OutputStream("RawWriter").Output = "DATAFILE='PFN:BooleTest.mdf' SVC='LHCb::RawDataCnvSvc' OPT='REC'"

#OutputStream("DigiWriter").Output = "DATAFILE='PFN:BooleTest.digi' TYP='POOL_ROOTTREE' OPT='REC'"


OutputStream("DstWriter").Output = "DATAFILE='PFN:BrunelTest.dst' TYP='POOL_ROOTTREE' OPT='REC'"

